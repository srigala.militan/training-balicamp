package id.co.balicamp.backend_service;

import id.co.balicamp.dto.Product;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by krisna putra on 10/27/2017.
 */
@FeignClient(value = "catalog",fallback = FallbackCatalogService.class)
public interface CatalogService {
    @GetMapping("/api/product/find/{name}")
    Product getDataByName(@PathVariable("name") String name);

}
@Component
 class FallbackCatalogService implements CatalogService{
    @Override
    public Product getDataByName(String name) {
        return null;
    }
}
