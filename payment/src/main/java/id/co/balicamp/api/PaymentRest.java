package id.co.balicamp.api;

import id.co.balicamp.backend_service.CatalogService;
import id.co.balicamp.dto.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by krisna putra on 10/27/2017.
 */
@RestController
@RequestMapping("/payment")
public class PaymentRest {

    @Autowired
    CatalogService catalogService;

    @GetMapping("/checkDiscountByProductId/{id}")
    public Map<String,Object> checkDiscountByProductId(@PathVariable("id") String id){
        Product p=catalogService.getDataByName(id);
        Map<String,Object> map=new HashMap<>();
        if(null!= p){
            map.put("product",p);
            map.put("discount",new Random().nextInt(100));
        }else{
            map.put("error","Product Not Found");
        }
        return map;
    }
}
