package id.co.balicamp.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by krisna putra on 10/27/2017.
 */
@Data
public class Product {
    private String id;
    private String code;
    private String name;
    private BigDecimal weigth;
}
