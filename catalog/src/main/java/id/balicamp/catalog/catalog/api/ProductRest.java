package id.balicamp.catalog.catalog.api;


import id.balicamp.catalog.catalog.dao.ProductDao;
import id.balicamp.catalog.catalog.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * Created by krisna putra on 10/26/2017.
 */
@RestController
@RequestMapping("/api/product")
public class ProductRest {
    @Value("${catalog.server.status}")
    private String status;
    @Autowired
    ProductDao dao;
    @GetMapping("/")
    public Iterable<Product> getListProduct(){
        return dao.findAll();
    }
    @PostMapping("/insert")
    public Product insert(@RequestBody Product p){
        return dao.save(p);
    }
    @PutMapping("/put/{id}")
    public Product put(@RequestBody Product product,@PathVariable("id") String id){
        Product product1=dao.findOne(id);
        product1.setCode(product.getCode());
        product1.setName(product.getName());
        product1.setWeigth(product.getWeigth());
        return dao.save(product1);
    }
    @PatchMapping("/patchNameById/{id}/{name}")
    public Product patchNameById(@PathVariable("id") String id,@PathVariable("name") String name){
        dao.updateNameById(name,id);
        return dao.findOne(id);
    }
    @GetMapping("/find/{name}")
    public Product getProduct(@PathVariable("name") Product p){
        return p;
    }

    @GetMapping("/status")
    public String getStatus(){
        return status;
    }
}
