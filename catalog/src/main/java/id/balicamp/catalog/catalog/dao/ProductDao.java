package id.balicamp.catalog.catalog.dao;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

/**
 * Created by krisna putra on 10/26/2017.
 */
public interface ProductDao extends PagingAndSortingRepository<id.balicamp.catalog.catalog.domain.Product,String> {

    @Transactional
    @Modifying
    @Query("update Product p set p.name=:name where p.id=:id")
    public int updateNameById(@Param("name") String name, @Param("id") String id);


}
